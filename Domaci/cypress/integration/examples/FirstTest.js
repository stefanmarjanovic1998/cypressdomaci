describe('MyTestSuite', () => {
    it('Go to the URL of a Page', function() 
    {
      cy.visit('https://skywalk.info/')
      Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
      })
      
      cy.get('#top-menu>#menu-item-3830>[href="https://skywalk.info/products/"]').invoke('show')
      cy.get('#menu-item-20424').click({ force: true })
      cy.get('#menu-item-20424').click()
      cy.url().should('eq','https://skywalk.info/project/tequila/') 
      //cy.get('#et_search_icon').click({ force: true })
      //cy.get('#et_search_icon').trigger('mouseover')
      //cy.get('/html/body/div[1]/header/div[1]/div[2]/div[1]/span').click()
      //cy.get('#top-menu>#menu-item-5713>a').click();
    })
    it('Search for a CHILI', function() 
    {
      cy.visit('https://skywalk.info/project/tequila/')
      Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
      })
      cy.get('#et_search_icon').click({ force: true })
      cy.get('.et-search-field').clear()
      cy.get('.et-search-field').type('CHILI').type('{enter}')
      cy.url().should('eq','https://skywalk.info/?s=CHILI') 
      cy.contains('CHILI4 – Limited Design – “Yellow”')
    })
    it('Search for a CHILY', function() 
    {
      cy.visit('https://skywalk.info/?s=CHILI')
      Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        // failing the test
        return false
      })
      cy.get('#et_search_icon').click({ force: true })
      cy.get('.et-search-field').clear()
      cy.get('.et-search-field').type('CHILY').type('{enter}')
      cy.url().should('eq','https://skywalk.info/?s=CHILY') 
      cy.contains('No Results Found')
    })
  })